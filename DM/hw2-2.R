library(cluster)
library(fpc)

#
# Try repeated k-means
#

# How many iterations for each k-means
iter_max <- 100

victim = read.table("2d-10c.dat", skip = 3)
# Dissimilarity matrix
dissE = daisy(victim)

km = list()
sk = list()

for (i in 1:20) {
  km[[i]] = kmeans(victim, centers=(i+4), iter.max=iter_max, nstart=iter_max/4)
  sk[[i]] = silhouette(km[[i]]$cl, dissE)
  plot(sk[[i]], main=sprintf("2d-10c silhouette after kmeans with %d centers", i))
}

# 
# km[[1]] = kmeans(victim, centers=5, iter.max=iter_max, nstart=iter_max/4)
# sk[[1]] = silhouette(km[[1]]$cl, dissE)
# plot(sk[[1]], main="2d-10c silhouette after kmeans with 5 centers")
# km[[2]] = kmeans(victim, centers=6, iter.max=iter_max, nstart=iter_max/4)
# sk[[2]] = silhouette(km[[2]]$cl, dissE)
# plot(sk[[2]], main="2d-10c silhouette after kmeans with 6 centers")
# km[[3]] = kmeans(victim, centers=7, iter.max=iter_max, nstart=iter_max/4)
# sk[[3]] = silhouette(km[[3]]$cl, dissE)
# plot(sk[[3]], main="2d-10c silhouette after kmeans with 7 centers")
# km[[4]] = kmeans(victim, centers=8, iter.max=iter_max, nstart=iter_max/4)
# sk[[4]] = silhouette(km[[4]]$cl, dissE)
# plot(sk[[4]], main="2d-10c silhouette after kmeans with 8 centers")
# km[[5]] = kmeans(victim, centers=9, iter.max=iter_max, nstart=iter_max/4)
# sk[[5]] = silhouette(km[[5]]$cl, dissE)
# plot(sk[[5]], main="2d-10c silhouette after kmeans with 9 centers")
# km[[6]] = kmeans(victim, centers=10, iter.max=iter_max, nstart=iter_max/4)
# sk[[6]] = silhouette(km[[6]]$cl, dissE)
# plot(sk[[6]], main="2d-10c silhouette after kmeans with 10 centers")
# km[[7]] = kmeans(victim, centers=11, iter.max=iter_max, nstart=iter_max/4)
# sk[[7]] = silhouette(km[[7]]$cl, dissE)
# plot(sk[[7]], main="2d-10c silhouette after kmeans with 11 centers")
# km[[8]] = kmeans(victim, centers=12, iter.max=iter_max, nstart=iter_max/4)
# sk[[8]] = silhouette(km[[8]]$cl, dissE)
# plot(sk[[8]], main="2d-10c silhouette after kmeans with 12 centers")


for(i in 1:20) {
  print(sprintf("centers: %d, silhouette width: %f", i+4, summary(sk[[i]])$avg.width))
}

clusplot(victim, km[[6]]$cluster, colors=TRUE, shade=TRUE, main="k-means with 10 centers")

#
# And now hierarchical
#
distmatrix <- dist(as.matrix(victim))
# Default Ward - single is slightly better
# hc <- hclust(distmatrix)
hc <- hclust(distmatrix, method="single")
plot(hc, main="Dendrogram with 8, 10 and 12 clusters marked")
#rect.hclust(hc, k=7, border="#800000")
rect.hclust(hc, k=8, border="#800000")
#rect.hclust(hc, k=9, border="#C00000")
rect.hclust(hc, k=10, border="#FF0000")
#rect.hclust(hc, k=11, border="#C00000")
rect.hclust(hc, k=12, border="#800000")
#rect.hclust(hc, k=13, border="#800000")
grouping <- cutree(hc, k=10)
plotcluster(victim, grouping, main="Clustering from dendrogram cut at 10 clusters")

# Display ratios between consecutive dendrogram levels
for (i in 2500:2523) { print (sprintf("%d, %f", 2523 - i, hc$height[i+1] / hc$height[i])) }
