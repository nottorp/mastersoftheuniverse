library(cluster)
library(fpc)

#
# Week 4 task
# Cojocaru Andrei
# Pavel Nicolae
# MOC 1
#
# Commented code is part of the attempts that proved useless
# We deleted some to keep the code:comments ratio above 1
#

#
# Iris data analysis
# kmeans - almost good
# dbscan works well though with euclidean, eps=1, lower misses points, higher finds one cluster
#

data(iris) # datele interne existente in R
dat <- iris[, -5] # fara clustering info
plot(dat)
clus <- kmeans(dat, centers=3) 
clus_dbscan <- dbscan(dat, eps=0.75, MinPts=5)
dat.dist = dist(dat, "euclidean")
clus_hc <- hclust(dat.dist)
plot(clus_dbscan, dat)
plot(clus_hc, hang=-1, label=iris$Species, main="iris hierarchical")
plotcluster(dat, clus$cluster,  main="iris kmeans")
plotcluster(dat, clus_dbscan$cluster,  main="iris dbscan eps=1")

#
# order2-3 data analysis
#
# kmeans - useless
# dbscan - useless
# hierarchical looks like it finds 3 clusters
#
ld =  read.csv("order2-3clust.csv")
dat <- ld[, -3]

plot(dat, main="order 2-3 original data")

wss <- (nrow(dat)-1)*sum(apply(dat,2,var))
for (i in 2:15) wss[i] <- sum(kmeans(dat,
                                     centers=i)$withinss)
plot(1:15, wss, type="b", xlab="Order 2-3 detecting number of clusters",
     ylab="Within groups sum of squares")

# Fail to detect no of clusters for kmeans - finds 2 not 3
#pamk.best <- pamk(dat)
#cat("number of clusters estimated by optimum average silhouette width:", pamk.best$nc, "\n")
# plot(pam(dat, pamk.best$nc), main="test 1")
#clusplot(pam(dat, pamk.best$nc), main="test 2")

# Failed attempts at trying various distance measures with dbscan
dat.dist = dist(dat, "euclidean")
clus_dbscan <- dbscan(dat.dist, eps=5, MinPts=3, method="dist" )
plot(clus_dbscan, dat, main="order2-3clust dbscan eps=3, dist=maximum")

dat.dist = dist(dat, "euclidean")
clus_hc <- hclust(dat.dist)
plot(clus_hc, hang=-1, main="order2-3clust hierarchical")
groups <- cutree(clus_hc, k=3) # cut tree into 2 clusters
# draw dendogram with red borders around the 2 clusters 
rect.hclust(clus_hc, k=3, border="red")
rect.hclust(clus_hc, k=4, border="red")

#
# 2d 10c data analysis
#
# kmeans - bad
# dbscan with eps=1 seems decent enough, but only finds 7 clusters
#
ld = read.table("2d-10c.dat", skip=3)
dat <- ld[, -3]
# clus <- kmeans(dat, centers=10)
# plotcluster(dat, clus$cluster, main="2d-10c kmean")
# clusplot(dat, clus$cluster, color=TRUE, shade=TRUE, labels=2, lines=0, main="2d-10c kmeans")
clus_dbscan = dbscan(dat, eps=0.5, MinPts=5)
plot(clus_dbscan, dat, main="2d-10c dbscan eps=0.5")
clus_dbscan = dbscan(dat, eps=0.8, MinPts=10)
plot(clus_dbscan, dat, main="2d-10c dbscan eps=0.75")
clus_dbscan = dbscan(dat, eps=1, MinPts=5)
plot(clus_dbscan, dat, main="2d-10c dbscan eps=1")
dist = dist(dat, "minkowski")
clus_dbscan_dist = dbscan(dist, eps=0.75, MinPts=10, method="dist")
plot(clus_dbscan_dist, dat, main="2d-10c dbscan distance")
# clus_dbscan = dbscan(dat, eps=1.25, MinPts=5)
# plot(clus_dbscan, dat, main="2d-10c dbscan eps=1.25")
# clus_dbscan = dbscan(dat, eps=1.5, MinPts=5)
# plot(clus_dbscan, dat, main="2d-10c dbscan eps=1.5")

wss <- (nrow(dat)-1)*sum(apply(dat,2,var))
for (i in 2:15) wss[i] <- sum(kmeans(dat,
                                     centers=i)$withinss)
plot(1:15, wss, type="b", xlab="2d10c detecting number of clusters",
     ylab="Within groups sum of squares")


#
# Smile data analysis
#
# dbscan with small epsilon and euclidean distance ain't bad
# 0.5 - misses a point
# 0.6 - is just right
# 0.75 - already finds just one cluster
#

ld = read.csv("smile.csv")
dat = ld[,-3]
# No need to cut rows out of this one
#km = kmeans(dat, centers=4)
clus_dbscan <- dbscan(dat, eps=0.6, MinPts=5)
plot(clus_dbscan, dat, main="smile dbscan")
#plotcluster(dat, sk, main="smile silhouette after kmeans with 4 centers")

#
# square data analysis - 4 clusters
# kmeans finds the clusters but misses the overlap?
# dbscan is ... confused
#
ld = read.table("square.data")
dat = ld[, -3]
km = kmeans(dat, centers=4)
plotcluster(dat, km$cluster, main="square kmeans")
dist = dist(dat, "euclidean") # tried various distances
clus_dbscan = dbscan(dist, eps=0.3, MinPts=5, method="dist")
plot(clus_dbscan, dat, main="square dbscan")

#
# long.data data analysis - 2 clusters
# kmeans is useless
# hierarchical does split in 2 clusters
#

ld= read.table("long.data")
dat = ld[, -3]
#km = kmeans(dat, centers=2)
#plot(km$cluster, main="long.data kmeans 2 centers")
#plotcluster(dat, km$cluster)
#clus_dbscan = dbscan(dat, eps=0.5, MinPts=5)
#plot(clus_dbscan, dat, main="long.data dbscan")
dat.dist = dist(dat, "euclidean")
clus_hc <- hclust(dat.dist)
plot(clus_hc, hang=-1, main="long.data hierarchical")
groups <- cutree(clus_hc, k=2) # cut tree into 2 clusters
# draw dendogram with red borders around the 2 clusters 
rect.hclust(clus_hc, k=2, border="red")
